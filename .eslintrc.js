module.exports = {
  root: true,
  extends: ['prettier', 'eslint-config-prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',
    'space-before-function-paren': 0
  }
}
