module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'none',
  semi: false,
  'eslint.autoFixOnSave': true,
  jsxSingleQuote: true
}
